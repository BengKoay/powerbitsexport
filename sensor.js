const device = [];
const sensor1 = {
	name: 'S1',
	modelType: 'AirOne Plus',
	uuidSource: '8926f120-ea58-4c19-a09b-bc2761319004',
	co2: false,
};
const sensor2 = {
	name: 'S2',
	modelType: 'AirOne Plus',
	uuidSource: '603bbed8-1e03-4168-b6bd-57c56685616a',
	co2: false,
};
const sensor3 = {
	name: 'S3',
	modelType: 'AirOne Plus',
	uuidSource: '86604e2f-b27f-44d1-b1f4-4a05fecd1bd8',
	co2: false,
};
const sensor4 = {
	name: 'S4',
	modelType: 'AirOne Plus',
	uuidSource: '8c36d9ce-72d5-4110-8a67-43028db559cc',
	co2: false,
};
const sensor5 = {
	name: 'S5',
	modelType: 'AirOne Plus',
	uuidSource: '40b3884c-70f6-4d02-9f4d-86f5809019cb',
	co2: false,
};
const sensor6 = {
	name: 'S6',
	modelType: 'AirOne Plus',
	uuidSource: 'cab43ede-80ea-4eb5-b851-38c2542badea',
	co2: false,
};
const sensor7 = {
	name: 'S7',
	modelType: 'AirOne Plus',
	uuidSource: '614f7f72-744d-4267-b881-f73e80d7b699',
	co2: false,
};
const sensor8 = {
	name: 'S8',
	modelType: 'AirOne Plus',
	uuidSource: '659f3809-1361-4b21-bc9a-1a63d17f0ab6',
	co2: false,
};
const sensor9 = {
	name: 'S9',
	modelType: 'AirOne Plus',
	uuidSource: '4fe60fe8-d752-4e7c-bcc5-96c48a79d187',
	co2: false,
};
const sensor10 = {
	name: 'S10',
	modelType: 'AirOne Plus',
	uuidSource: '967cad92-7b88-46f6-ae38-385eb8e4149d',
	co2: false,
};
const sensor11 = {
	name: 'S11',
	modelType: 'AirOne Plus',
	uuidSource: '191400d7-fb9a-4f0b-9ef8-5f7fd064ebc2',
	co2: false,
};
const sensor12 = {
	name: 'S12',
	modelType: 'AirOne Plus',
	uuidSource: 'd9ba978b-22ae-42d3-8170-88d198ffd61a',
	co2: false,
};
const sensor13 = {
	name: 'S13',
	modelType: 'AirOne Plus',
	uuidSource: 'dee4a64e-b7d7-49f8-bf31-b5ccd5e86d1f',
	co2: true,
};
const sensor14 = {
	name: 'S14',
	modelType: 'AirOne Plus',
	uuidSource: 'b984e055-a9f4-4fea-97ff-4bdd67f45877',
	co2: true,
};
const sensor15 = {
	name: 'S15',
	modelType: 'AirOne Plus',
	uuidSource: 'a98e243b-1dfe-469e-b13f-94cff49864c5',
	co2: true,
};

device.push(sensor1);
device.push(sensor2);
device.push(sensor3);
device.push(sensor4);
device.push(sensor5);
device.push(sensor6);
device.push(sensor7);
device.push(sensor8);
device.push(sensor9);
device.push(sensor10);
device.push(sensor11);
// device.push(sensor12);
device.push(sensor13);
device.push(sensor14);
// device.push(sensor15);

const directory = './export';
const XLSX = require('xlsx');
const moment = require('moment');
const axios = require('axios');
const { aws4Interceptor } = require('aws4-axios');
// const ObjectsToCsv = require('objects-to-csv');
// const selectYear = '2021';
// const selectMonth = '10';
const selectMonth = moment('2021-11', 'YYYY-MM');
const daysInMonth = selectMonth.daysInMonth();

// const deviceId = 'b176cf1e-a537-46c9-8ae7-e7b27a3de5c7';
// const deviceId = '8926f120-ea58-4c19-a09b-bc2761319004';
const start = moment().subtract(1, 'day').unix();
const end = moment().unix();

// const deviceId = 'c4af4daa-15fc-45fd-a922-e539f2c361f1';
const axiosSolar = {
	baseURL: 'https://9ry30zin92.execute-api.ap-southeast-1.amazonaws.com/public',
	apiKey: 'eJzWdiiQi18jzBIWxqFHV1TRAgfcIg8h8Lc0wgBB',
	region: 'ap-southeast-1',
	service: 'execute-api',
	accessKeyId: 'AKIAVBSYQGXZE6POVWVK',
	secretAccessKey: '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',
};
const axiosMainHelper = axios.create({
	baseURL: axiosSolar.baseURL,
	headers: { 'x-api-key': axiosSolar.apiKey },
});
const interceptor = aws4Interceptor(
	{
		region: axiosSolar.region,
		service: axiosSolar.service,
	},
	{
		accessKeyId: axiosSolar.accessKeyId,
		secretAccessKey: axiosSolar.secretAccessKey,
	}
);
axiosMainHelper.interceptors.request.use(interceptor);

const customerName = 'JB Cocoa Sdn Bhd';
let exportFileName = `${directory}/${customerName}_sensor_${selectMonth.format(
	'YYYY-MM-DD'
)}.xlsx`;
const main = async () => {
	let wb = XLSX.utils.book_new();
	console.log(daysInMonth);
	try {
		// loop device
		for (const eachDevice of device) {
			let pushData = [];
			// loop days
			for (let day = 0; day < daysInMonth; day++) {
				let startOfDay = moment(selectMonth)
					.startOf('day')
					.add(day, 'days')
					.unix();
				let endOfDay = moment(selectMonth).endOf('day').add(day, 'days').unix();

				//   const response = await axiosMainHelper.post('/rawtmpdata', JSON.stringify( data));
				const response = await axiosMainHelper.get(
					`/rawtmpdata/?deviceID=${eachDevice.uuidSource}&start_time=${startOfDay}000&end_time=${endOfDay}000`
				);
				// console.log(response.header);
				console.log(startOfDay, endOfDay);
				//   console.log(response.data['0']);
				// loop each row of data
				for (const row in response.data) {
					//   console.log(JSON.parse(response.data[`${row}`].stringifiedData.S), row);
					let dataPoint = JSON.parse(response.data[`${row}`].stringifiedData.S);
					if (
						!!dataPoint

						// !!eachDataPoint.sensor.quotient &&
						// !!eachDataPoint.sensor.moisture &&
						// !!eachDataPoint.sensor.temperature
					) {
						eachDataPoint = {
							Time: moment.unix(dataPoint.timestamp).format('YYYY-MM-DD HH:mm'),
							'Device Name': dataPoint.deviceName,
							Temperature: dataPoint.temperature,
							Humidity: dataPoint.humidity,
						};
						//   console.log(pushData)
						if (!!dataPoint.co2) {
							// console.log(dataPoint.co2);
							eachDataPoint = { ...eachDataPoint, CO2: dataPoint.co2 };
						}
						if (
							!!dataPoint.AQI &&
							dataPoint.AQI.hasOwnProperty('PM_SP_UG_2_5')
						) {
							// console.log(dataPoint.AQI.PM_SP_UG_2_5);
							eachDataPoint = {
								...eachDataPoint,
								PM_SP_UG_2_5: dataPoint.AQI.PM_SP_UG_2_5,
							};
						}
						pushData.push(eachDataPoint);
					}
				}

				// const csv = new ObjectsToCsv(pushData);

				// await csv.toDisk(`./JBsensor.csv`);
				//   console.log(pushData);
			}
			// write JSON data to sheet
			let ws = XLSX.utils.json_to_sheet(pushData);
			let eachDeviceName = eachDevice.name;
			// if (eachDeviceName == 'Blk Cool Room A/C')
			//   eachDeviceName = 'Blk Cool Room AC';
			XLSX.utils.book_append_sheet(wb, ws, eachDeviceName);
		}
		XLSX.writeFile(wb, exportFileName);
	} catch (e) {
		console.log(e);
	}
};

main();
