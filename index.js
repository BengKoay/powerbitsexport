const { Pool, Client } = require('pg');
const moment = require('moment');
const XLSX = require('xlsx');

// 'Chiller 3',                                      // MSB 2
// 'Chiller 4 (Trane)',                              // MSB 2
// 'ZT-160D Compressor',                             // MSB 2
// 'ZT-160A Compressor',                             // MSB 2
// 'Smartech Chiller 7A & 7B',                       // MSB 3
// 'Hitachi Chiller 6',                              // MSB 3
// 'Sullair Compressor',                             // MSB 3
// 'Trane Chiller 5',                                // MSB 3
// 'ZT-160B Compressor',                             // MSB 3
// 'ZT-160C Compressor',                             // MSB 3
// 'PPPL3',                                          // MSB 3
// 'PPPL2',                                          // MSB 3
// 'DEGUM 2 Chiller',                                // MSB 3
// 'Blk Cool Room A/C',                              // MSB 4

// JB COCOA DEVICE LIST
// "a3c306ef-09f8-4eab-a8e3-fdcafb036716"	"Chiller 4 (Trane)"
// "e32dc39b-fd7d-4374-99d9-7ee34b5acfbc"	"Smartech Chiller 7A & 7B"
// "e4a7f0e6-4e27-4068-aa93-69e9e4fc5a84"	"ZT-160C Compressor"
// "c859239e-1c9a-4703-9350-1401b5a302d1"	"ZT-160D Compressor"
// "80eeacb2-5b17-492e-b08a-f9351a142ba4"	"Sullair Compressor"
// "ad09068c-1912-4c2e-8826-77d02840eb67"	"MSB 3"
// "782cd5b3-e669-433e-a810-336f4ea72d19"	"MSB 4"
// "11c8910e-30a1-4368-b6f5-b514fe29f826"	"PPPL3"
// "48ef91db-a076-4612-95a3-4f27e9e42dca"	"PPPL2"
// "9d171929-b028-4a21-9fa1-fbd49a17ea92"	"DEGUM 2 Chiller"
// "335e48e0-6110-4c84-8bb1-6d6b59a7fbc5"	"MSB 2"
// "98be0649-1d97-4c62-b806-4377c3cbb882"	"Hitachi Chiller 6"
// "ecb0abe2-6687-47da-be00-67fe3a64b895"	"ZT-160A Compressor"
// "babf6ef4-2dae-4697-a734-e361d363ccf6"	"MSB 1"
// "ccedeaee-1640-4de8-9e46-0f1ea16f5094"	"Trane Chiller 5"
// "47dfbb74-19d8-496d-96bd-782fea49110a"	"Blk Cool Room A/C"
// "dd37a310-ae5b-4f99-8e06-ef7d94d93c54"	"ZT-160B Compressor"
// "1b9e481f-a3aa-4f99-8ab5-44d518bf3dc6"	"Chiller 3"

// ELEGANT DEVICE LIST
// "9b9c613b-ab66-4653-b020-ba9fa3f351f4"	"DB-EQ"
// "aa381915-fc57-408f-817a-51cde9ab5244"	"DB SPA AC"
// "787ae668-0a4d-4f58-9502-becda2996ca2"	"DB SPA L"
// "21626eac-48b5-4904-b964-02d62c2f1e4e"	"DB SPA LI"
// "a18ec681-c29b-4d65-b11d-459a454d0441"	"LOT1-1"
// "0ce614ce-64dc-4efa-9016-b2e0a634edb6"	"LOT1-2"
// "020c63bd-0717-4f01-9134-b66d1529d24c"	"LOT1-3"
// "80557806-617e-4876-b9be-84a9087e93ee"	"LOT1-G"
// "92c9c951-ebef-4111-ae0f-e1aa7a2ff984"	"LOT3-1"
// "7056e870-a44b-43b4-a477-957eee317097"	"LOT3-2"
// "ef800882-a8c5-4c0c-ad49-7d6eddb41572"	"LOT3-3"
// "01acfa61-a3bb-4ce3-8988-51fea0972627"	"LOT3-G"
// "3b33f172-46ed-494b-814c-e3bf70361204"	"LOT5-1"
// "87c71264-dc19-49fb-867e-7d20146097e9"	"LOT5-2"
// "3951a042-200b-4eb7-8842-7c50236b5bb6"	"LOT5-3"
// "269cd560-c72b-46a6-9416-0623d40cdcc7"	"LOT5-4"
// "5fac914f-f035-4b84-aa86-7b35a4f50f05"	"LOT5-G"
// "35a3500e-7f10-4966-9c68-135f0135fb4c"	"LOT7-1"
// "fa29b3dc-5f49-4fcf-b889-abab396918e8"	"LOT7-2"
// "e5976537-e8a3-43d8-9028-ac8c8c46541f"	"LOT7-3"
// "4ffb6352-9a56-4720-ac9f-3968c2c42bda"	"LOT7-4"
// "486ad3ff-7d3a-442f-a8f0-87eb560e70c6"	"LOT7-G-1"
// "c848c15a-af9b-498b-95f2-056d63cf8e5d"	"LOT7-G-2"
// "39513140-9327-4361-87fa-93586ea25e1a"	"LOT9-1"
// "d0b50943-768f-4397-8934-2fe934424fd2"	"LOT9-2"
// "14293212-1ef6-44b3-b868-25dc9372130a"	"LOT9-3"
// "f4b9e4a5-b114-4da9-8190-921cda823bc2"	"LOT9-4"
// "b3b31fdc-4630-472d-913f-db6d5f1681e4"	"LOT9-G-1"
// "48896243-9c57-4123-ab0f-f8366dba9ac1"	"LOT9-G-2"

const directory = './export';
const deviceAll = [
	// 'MSB 1',
	// 'MSB 2',
	// 'MSB 3',
	// 'MSB 4',
	// 'Chiller 3', // MSB 2
	// 'Chiller 4 (Trane)', // MSB 2
	// 'ZT-160D Compressor', // MSB 2
	// 'ZT-160A Compressor', // MSB 2
	// 'Smartech Chiller 7A & 7B', // MSB 3
	// 'Hitachi Chiller 6', // MSB 3
	// 'Sullair Compressor', // MSB 3
	// 'Trane Chiller 5', // MSB 3
	// 'ZT-160B Compressor', // MSB 3
	// 'ZT-160C Compressor', // MSB 3
	// 'PPPL3', // MSB 3
	// 'PPPL2', // MSB 3
	// 'DEGUM 2 Chiller', // MSB 3
	// 'Blk Cool Room A/C', // MSB 4
	'babf6ef4-2dae-4697-a734-e361d363ccf6', //	"MSB 1"
	'335e48e0-6110-4c84-8bb1-6d6b59a7fbc5', //	"MSB 2"
	'ad09068c-1912-4c2e-8826-77d02840eb67', //	"MSB 3"
	'782cd5b3-e669-433e-a810-336f4ea72d19', //	"MSB 4"
	'1b9e481f-a3aa-4f99-8ab5-44d518bf3dc6', //	"Chiller 3"
	'a3c306ef-09f8-4eab-a8e3-fdcafb036716', //	"Chiller 4 (Trane)"
	'c859239e-1c9a-4703-9350-1401b5a302d1', //	"ZT-160D Compressor"
	'ecb0abe2-6687-47da-be00-67fe3a64b895', //	"ZT-160A Compressor"
	'e32dc39b-fd7d-4374-99d9-7ee34b5acfbc', //	"Smartech Chiller 7A & 7B"
	'98be0649-1d97-4c62-b806-4377c3cbb882', //	"Hitachi Chiller 6"
	'80eeacb2-5b17-492e-b08a-f9351a142ba4', //	"Sullair Compressor"
	'ccedeaee-1640-4de8-9e46-0f1ea16f5094', //	"Trane Chiller 5"
	'dd37a310-ae5b-4f99-8e06-ef7d94d93c54', //	"ZT-160B Compressor"
	'e4a7f0e6-4e27-4068-aa93-69e9e4fc5a84', //	"ZT-160C Compressor"
	'11c8910e-30a1-4368-b6f5-b514fe29f826', //	"PPPL3"
	'48ef91db-a076-4612-95a3-4f27e9e42dca', //	"PPPL2"
	'9d171929-b028-4a21-9fa1-fbd49a17ea92', //	"DEGUM 2 Chiller"
	'47dfbb74-19d8-496d-96bd-782fea49110a', //	"Blk Cool Room A/C"
	// "9b9c613b-ab66-4653-b020-ba9fa3f351f4",//	"DB-EQ"
	// "aa381915-fc57-408f-817a-51cde9ab5244",//	"DB SPA AC"
	// "787ae668-0a4d-4f58-9502-becda2996ca2",//	"DB SPA L"
	// "21626eac-48b5-4904-b964-02d62c2f1e4e",//	"DB SPA LI"
	// "a18ec681-c29b-4d65-b11d-459a454d0441",//	"LOT1-1"
	// "0ce614ce-64dc-4efa-9016-b2e0a634edb6",//	"LOT1-2"
	// "020c63bd-0717-4f01-9134-b66d1529d24c",//	"LOT1-3"
	// "80557806-617e-4876-b9be-84a9087e93ee",//	"LOT1-G"
	// "92c9c951-ebef-4111-ae0f-e1aa7a2ff984",//	"LOT3-1"
	// "7056e870-a44b-43b4-a477-957eee317097",//	"LOT3-2"
	// "ef800882-a8c5-4c0c-ad49-7d6eddb41572",//	"LOT3-3"
	// "01acfa61-a3bb-4ce3-8988-51fea0972627",//	"LOT3-G"
	// "3b33f172-46ed-494b-814c-e3bf70361204",//	"LOT5-1"
	// "87c71264-dc19-49fb-867e-7d20146097e9",//	"LOT5-2"
	// "3951a042-200b-4eb7-8842-7c50236b5bb6",//	"LOT5-3"
	// "269cd560-c72b-46a6-9416-0623d40cdcc7",//	"LOT5-4"
	// "5fac914f-f035-4b84-aa86-7b35a4f50f05",//	"LOT5-G"
	// "35a3500e-7f10-4966-9c68-135f0135fb4c",//	"LOT7-1"
	// "fa29b3dc-5f49-4fcf-b889-abab396918e8",//	"LOT7-2"
	// "e5976537-e8a3-43d8-9028-ac8c8c46541f",//	"LOT7-3"
	// "4ffb6352-9a56-4720-ac9f-3968c2c42bda",//	"LOT7-4"
	// "486ad3ff-7d3a-442f-a8f0-87eb560e70c6",//	"LOT7-G-1"
	// "c848c15a-af9b-498b-95f2-056d63cf8e5d",//	"LOT7-G-2"
	// "39513140-9327-4361-87fa-93586ea25e1a",//	"LOT9-1"
	// "d0b50943-768f-4397-8934-2fe934424fd2",//	"LOT9-2"
	// "14293212-1ef6-44b3-b868-25dc9372130a",//	"LOT9-3"
	// "f4b9e4a5-b114-4da9-8190-921cda823bc2",//	"LOT9-4"
	// "b3b31fdc-4630-472d-913f-db6d5f1681e4",//	"LOT9-G-1"
	// "48896243-9c57-4123-ab0f-f8366dba9ac1",//	"LOT9-G-2"
];

const tableName = 'reading_smart_meter';
const customerName = 'JB Cocoa Sdn Bhd';
// const customerName = 'Elegant Group Sdn Bhd';
const lotId = '4259d53c-540a-44e2-a958-650c4e02c0cb'; // JB COCOA
// const lotId = 'ca71c871-a27d-43dd-ac49-a921bcbd2cb1'; // ELEGANT
const sqlConditionDateStart = '2021-11-01';
const sqlConditionDateEnd = '2021-11-30';

let exportFileName = `${directory}/${customerName}_${sqlConditionDateStart}_${sqlConditionDateEnd}.xlsx`;

// let sampleQuery1 = `

// WITH res AS (
//     SELECT *
//          , ROW_NUMBER() OVER (PARTITION BY reading_smart_meter."deviceName", date_trunc('hour', reading_smart_meter."transDate") ORDER BY reading_smart_meter."transDate" ASC) as rn
//       FROM reading_smart_meter WHERE  "customerName" = 'JB Cocoa Sdn Bhd' AND ("deviceName" = 'MSB 1'  ) AND "transDate" BETWEEN '2021-05-01  00:00:00' and '2021-06-30 23:59:59'
// )
// SELECT *
//   FROM res
//  WHERE rn = 1`;

// let sampleQuery2 = `
// SELECT
// "transDate","activeFwdEnergy","activeRevEnergy","totalActivePower","customerName","deviceName","voltage1","voltage2","voltage3","current1","current2","current3","powerFactor","frequency","maxDemand"
// FROM  ${tableName}
// WHERE ("customerName" = '${customerName}' AND "deviceName" = '${eachDevice}')
// AND "transDate" BETWEEN '${sqlConditionDateStart} 00:00:00' and '${sqlConditionDateEnd} 23:59:59'
// ORDER BY "transDate" ASC
// `;

const allDevices = [];
const main = async () => {
	let wb = XLSX.utils.book_new();
	for (const eachDevice of deviceAll) {
		// exportFileName = `${directory}/eachDevice_${sqlConditionDateStart}_${sqlConditionDateEnd}month.xlsx`;
		const writeToCSV = [];
		console.log(eachDevice);
		const pool = await new Pool({
			user: 'hardware_engineer',
			host: 'source-db.cluster-ro-cqijxs6doddf.ap-southeast-1.rds.amazonaws.com',
			database: 'sourcenavidb',
			password: 'rKi!LK8G8@coWkjZ',
			port: 5432,
		});

		let sqlString = `

    WITH res AS (
        SELECT *
             , ROW_NUMBER() OVER (PARTITION BY reading_smart_meter."deviceName", date_trunc('hour', reading_smart_meter."transDate") ORDER BY reading_smart_meter."transDate" ASC) as rn
          FROM reading_smart_meter WHERE  "lotId" = '${lotId}' AND ("smartMeterId" = '${eachDevice}') AND "transDate" BETWEEN '${sqlConditionDateStart}  00:00:00' and '${sqlConditionDateEnd} 23:59:59'
    )
    SELECT *
      FROM res
     WHERE rn = 1`;

		//    "voltage1","voltage2","voltage3","current1","current2","current3","powerFactor","frequency",

		// let sqlString = `
		// SELECT
		// "transDate","activeFwdEnergy","activeRevEnergy","totalActivePower","customerName","deviceName","maxDemand"
		// FROM  ${tableName}
		// WHERE ("customerName" = '${customerName}' AND "deviceName" = '${eachDevice}')
		// AND "transDate" BETWEEN '${sqlConditionDateStart} 00:00:00' and '${sqlConditionDateEnd} 23:59:59'
		// ORDER BY "transDate" ASC
		// `;

		let queryResult = await pool.query(sqlString);
		if (!!queryResult.err) console.log('error ', queryResult.err);
		console.log(sqlString);
		await pool.end();
		let eachDeviceName2 = '';

		for (const row of queryResult.rows) {
			{
				let tempData = {
					Timestamp: moment(row.transDate).format('YYYY-MM-DD HH:mm'),
					'Utility Intake (kW)': row.totalActivePower,
					'Utility Import (kWh)': row.activeFwdEnergy,
					'Utility Max Demand (kW)': row.maxDemand,
					'Utility Power Factor': row.powerFactor,
					'Utility Voltage1 (V)': row.voltage1,
					'Utility Voltage2 (V)': row.voltage2,
					'Utility Voltage3 (V)': row.voltage3,
					'Utility Current1 (A)': row.current1,
					'Utility Current2 (A)': row.current2,
					'Utility Current3 (A)': row.current3,
					'Utility Frequency (Hz)': row.frequency,
					'Customer Name': row.customerName,
					'Device Name': row.deviceName,
				};
				eachDeviceName2 = row.deviceName;
				await writeToCSV.push(tempData);
			}
		}
		let ws = XLSX.utils.json_to_sheet(writeToCSV);
		ws['!cols'] = [
			{ wch: 15 },
			{ wch: 15 },
			{ wch: 15 },
			{ wch: 15 },
			{ wch: 15 },
			{ wch: 15 },
			{ wch: 15 },
			{ wch: 15 },
			{ wch: 15 },
			{ wch: 15 },
			{ wch: 15 },
			{ wch: 15 },
			{ wch: 15 },
			{ wch: 15 },
			{ wch: 15 },
		];
		// let eachDeviceName = eachDevice;

		// if (eachDeviceName == 'Blk Cool Room A/C')
		// 	eachDeviceName = 'Blk Cool Room AC';

		// switch(eachDeviceName) {
		// 	case 'dd37a310-ae5b-4f99-8e06-ef7d94d93c54':
		// 		eachDeviceName = 'ZT-160B Compressor';
		// 		break;
		// 	case '1b9e481f-a3aa-4f99-8ab5-44d518bf3dc6':
		// 		eachDeviceName = 'Chiller 3';
		// 		break;
		// 	default:
		// 		eachDeviceName = 'Unknown';
		// }

		eachDeviceName = eachDeviceName2.replace('/', '');

		XLSX.utils.book_append_sheet(wb, ws, eachDeviceName);
		// XLSX.writeFile(wb, `${directory}/${eachDevice}.xlsx`);
		// XLSX.writeFile(wb, exportFileName);
		// allDevices.push(writeToCSV);
	}
	// console.log(outputData);

	// for (const eachdevices of allDevices)
	//   for (const row of eachdevices) {
	//     console.log(
	//       moment(row['Timestamp']).hour(),
	//       moment(row['Timestamp']).minute()
	//     );
	//     // moment(row['Timestamp']).hour;
	//     console.log(count);
	//     count++;
	//   }
	XLSX.writeFile(wb, exportFileName);
};
main();

// const { Pool, Client } = require('pg');
// const moment = require('moment');
// const XLSX = require('xlsx');

// // 'Chiller 3',                                      // MSB 2
// // 'Chiller 4 (Trane)',                              // MSB 2
// // 'ZT-160D Compressor',                             // MSB 2
// // 'ZT-160A Compressor',                             // MSB 2
// // 'Smartech Chiller 7A & 7B',                       // MSB 3
// // 'Hitachi Chiller 6',                              // MSB 3
// // 'Sullair Compressor',                             // MSB 3
// // 'Trane Chiller 5',                                // MSB 3
// // 'ZT-160B Compressor',                             // MSB 3
// // 'ZT-160C Compressor',                             // MSB 3
// // 'PPPL3',                                          // MSB 3
// // 'PPPL2',                                          // MSB 3
// // 'DEGUM 2 Chiller',                                // MSB 3
// // 'Blk Cool Room A/C',                              // MSB 4

// const directory = './export';
// const deviceAll = [
// 	'MSB 1',
// 	'MSB 2',
// 	'MSB 3',
// 	'MSB 4',
// 	'Chiller 3', // MSB 2
// 	'Chiller 4 (Trane)', // MSB 2
// 	'ZT-160D Compressor', // MSB 2
// 	'ZT-160A Compressor', // MSB 2
// 	'Smartech Chiller 7A & 7B', // MSB 3
// 	'Hitachi Chiller 6', // MSB 3
// 	'Sullair Compressor', // MSB 3
// 	'Trane Chiller 5', // MSB 3
// 	'ZT-160B Compressor', // MSB 3
// 	'ZT-160C Compressor', // MSB 3
// 	'PPPL3', // MSB 3
// 	'PPPL2', // MSB 3
// 	'DEGUM 2 Chiller', // MSB 3
// 	'Blk Cool Room A/C', // MSB 4
// ];

// const tableName = 'reading_smart_meter';
// const customerName = 'JB Cocoa Sdn Bhd';
// const sqlConditionDateStart = '2021-10-01';
// const sqlConditionDateEnd = '2021-10-31';

// let exportFileName = `${directory}/${customerName}_${sqlConditionDateStart}_${sqlConditionDateEnd}.xlsx`;

// // let sampleQuery1 = `

// // WITH res AS (
// //     SELECT *
// //          , ROW_NUMBER() OVER (PARTITION BY reading_smart_meter."deviceName", date_trunc('hour', reading_smart_meter."transDate") ORDER BY reading_smart_meter."transDate" ASC) as rn
// //       FROM reading_smart_meter WHERE  "customerName" = 'JB Cocoa Sdn Bhd' AND ("deviceName" = 'MSB 1'  ) AND "transDate" BETWEEN '2021-05-01  00:00:00' and '2021-06-30 23:59:59'
// // )
// // SELECT *
// //   FROM res
// //  WHERE rn = 1`;

// // let sampleQuery2 = `
// // SELECT
// // "transDate","activeFwdEnergy","activeRevEnergy","totalActivePower","customerName","deviceName","voltage1","voltage2","voltage3","current1","current2","current3","powerFactor","frequency","maxDemand"
// // FROM  ${tableName}
// // WHERE ("customerName" = '${customerName}' AND "deviceName" = '${eachDevice}')
// // AND "transDate" BETWEEN '${sqlConditionDateStart} 00:00:00' and '${sqlConditionDateEnd} 23:59:59'
// // ORDER BY "transDate" ASC
// // `;

// const allDevices = [];
// const main = async () => {
// 	let wb = XLSX.utils.book_new();
// 	for (const eachDevice of deviceAll) {
// 		// exportFileName = `${directory}/eachDevice_${sqlConditionDateStart}_${sqlConditionDateEnd}month.xlsx`;
// 		const writeToCSV = [];
// 		console.log(eachDevice);
// 		const pool = await new Pool({
// 			user: 'hardware_engineer',
// 			host: 'source-db.cluster-ro-cqijxs6doddf.ap-southeast-1.rds.amazonaws.com',
// 			database: 'sourcenavidb',
// 			password: 'rKi!LK8G8@coWkjZ',
// 			port: 5432,
// 		});

// 		let sqlString = `

//     WITH res AS (
//         SELECT *
//              , ROW_NUMBER() OVER (PARTITION BY reading_smart_meter."deviceName", date_trunc('hour', reading_smart_meter."transDate") ORDER BY reading_smart_meter."transDate" ASC) as rn
//           FROM reading_smart_meter WHERE  "customerName" = '${customerName}' AND ("deviceName" = '${eachDevice}'  ) AND "transDate" BETWEEN '${sqlConditionDateStart}  00:00:00' and '${sqlConditionDateEnd} 23:59:59'
//     )
//     SELECT *
//       FROM res
//      WHERE rn = 1`;
// 		//    "voltage1","voltage2","voltage3","current1","current2","current3","powerFactor","frequency",

// 		// let sqlString = `
// 		// SELECT
// 		// "transDate","activeFwdEnergy","activeRevEnergy","totalActivePower","customerName","deviceName","maxDemand"
// 		// FROM  ${tableName}
// 		// WHERE ("customerName" = '${customerName}' AND "deviceName" = '${eachDevice}')
// 		// AND "transDate" BETWEEN '${sqlConditionDateStart} 00:00:00' and '${sqlConditionDateEnd} 23:59:59'
// 		// ORDER BY "transDate" ASC
// 		// `;

// 		let queryResult = await pool.query(sqlString);
// 		if (!!queryResult.err) console.log('error ', queryResult.err);
// 		console.log(sqlString);
// 		await pool.end();

// 		for (const row of queryResult.rows) {
// 			{
// 				let tempData = {
// 					Timestamp: moment(row.transDate).format('YYYY-MM-DD HH:mm'),
// 					'Utility Intake (kW)': row.totalActivePower,
// 					'Utility Import (kWh)': row.activeFwdEnergy,
// 					'Utility Max Demand (kW)': row.maxDemand,
// 					'Utility Power Factor': row.powerFactor,
// 					'Utility Voltage1 (V)': row.voltage1,
// 					'Utility Voltage2 (V)': row.voltage2,
// 					'Utility Voltage3 (V)': row.voltage3,
// 					'Utility Current1 (A)': row.current1,
// 					'Utility Current2 (A)': row.current2,
// 					'Utility Current3 (A)': row.current3,
// 					'Utility Frequency (Hz)': row.frequency,
// 					'Customer Name': row.customerName,
// 					'Device Name': row.deviceName,
// 				};
// 				await writeToCSV.push(tempData);
// 			}
// 		}
// 		let ws = XLSX.utils.json_to_sheet(writeToCSV);
// 		ws['!cols'] = [
// 			{ wch: 15 },
// 			{ wch: 15 },
// 			{ wch: 15 },
// 			{ wch: 15 },
// 			{ wch: 15 },
// 			{ wch: 15 },
// 			{ wch: 15 },
// 			{ wch: 15 },
// 			{ wch: 15 },
// 			{ wch: 15 },
// 			{ wch: 15 },
// 			{ wch: 15 },
// 			{ wch: 15 },
// 			{ wch: 15 },
// 			{ wch: 15 },
// 		];
// 		let eachDeviceName = eachDevice;
// 		if (eachDeviceName == 'Blk Cool Room A/C')
// 			eachDeviceName = 'Blk Cool Room AC';
// 		XLSX.utils.book_append_sheet(wb, ws, eachDeviceName);
// 		// XLSX.writeFile(wb, `${directory}/${eachDevice}.xlsx`);
// 		// XLSX.writeFile(wb, exportFileName);
// 		// allDevices.push(writeToCSV);
// 	}
// 	// console.log(outputData);

// 	// for (const eachdevices of allDevices)
// 	//   for (const row of eachdevices) {
// 	//     console.log(
// 	//       moment(row['Timestamp']).hour(),
// 	//       moment(row['Timestamp']).minute()
// 	//     );
// 	//     // moment(row['Timestamp']).hour;
// 	//     console.log(count);
// 	//     count++;
// 	//   }
// 	XLSX.writeFile(wb, exportFileName);
// };
// main();
